//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#include "basecombatcharacter.h"
#include "Sprite.h"
#include "rumble_shared.h"

#define BOLT_AIR_VELOCITY	2500
#define BOLT_WATER_VELOCITY	1500

//-----------------------------------------------------------------------------
// Crossbow Bolt
//-----------------------------------------------------------------------------
class CCrossbowBolt : public CBaseCombatCharacter
{
	DECLARE_CLASS(CCrossbowBolt, CBaseCombatCharacter);

public:
	CCrossbowBolt();
	~CCrossbowBolt();

	Class_T Classify(void) { return CLASS_NONE; }

public:
	void Spawn(void);
	void Precache(void);
	void BubbleThink(void);
	void BoltTouch(CBaseEntity *pOther);
	bool CreateVPhysics(void);
	unsigned int PhysicsSolidMaskForEntity() const;
	static CCrossbowBolt *BoltCreate(const Vector &vecOrigin, const QAngle &angAngles, CBasePlayer *pentOwner = NULL, bool isharpoon = false );

protected:

	bool	CreateSprites(void);

	CHandle<CSprite>		m_pGlowSprite;
	//CHandle<CSpriteTrail>	m_pGlowTrail;

	bool m_bIsHarpoon;

	DECLARE_DATADESC();
	DECLARE_SERVERCLASS();
};