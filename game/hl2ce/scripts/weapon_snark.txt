// Snark

WeaponData
{
	// Weapon data is loaded by both the Game and Client DLLs.
	"printname"		"#WPNHUD_Snark"
	"viewmodel"		"models/weapons/v_squeak.mdl"
	"playermodel"		"models/weapons/w_squeak.mdl"
	"anim_prefix"		"squeak"
	"bucket"		"5"
	"bucket_position"	"2"

	"clip_size"		"-1"
	"default_clip"		"5"
	"primary_ammo"		"Snark"
	"secondary_ammo"	"None"

	"weight"		"5"
	"item_flags"		"24"	// ITEM_FLAG_LIMITINWORLD | ITEM_FLAG_EXHAUSTIBLE

	"addfov" "36"

	// Sounds for the weapon. There is a max of 16 sounds per category (i.e. max 16 "single_shot" sounds)
	SoundData
	{
	}

	// Weapon Sprite data is loaded by the Client DLL.
	TextureData
	{
		"weapon"
		{
				"font"		"NewWeaponIcons"
				"character"	"j"
		}
		"weapon_s"
		{	
				"font"		"NewWeaponIconsSelected"
				"character"	"j"
		}
		"weapon_small"
		{
				"font"		"NewWeaponIconsSmall"
				"character"	"j"
		}
		"ammo"
		{
			"file"		"sprites/640hud7"
			"x"			"96"
			"y"			"96"
			"width"		"24"
			"height"	"24"
		}
		"crosshair"
		{
				"font"		"Crosshairs"
				"character"	"Q"
		}
		"autoaim"
		{
				"file"		"sprites/crosshairs"
				"x"			"0"
				"y"			"48"
				"width"		"24"
				"height"	"24"
		}
	}
}