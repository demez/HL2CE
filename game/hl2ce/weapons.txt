implemented:
	melee:
		crowbar: normal hl2
		stunstick: normal hl2:dm

	onehanded:
		pistol: 2 modes, normal hl2 and akimbo (for when picking up a second one, sec attack to swap between single/akimbo)
		357: normal hl2
		alyxgun: 2 modes, 3rnd burst and full auto (sec attack to swap)

	lightarms:
		smg1: normal hl2
		ar2: normal hl2
		shotgun: normal hl2
		autosthogun: semi-auto shotgun, deals slightly less damage but fires much faster+holds more rounds

	heavyarms:
		crossbow: normal hl2
		annabelle: accurate but slow
		dartbow: hl1 crossbow
		
	special:
		rpg: normal hl2
		gauss: normal hl1 (hl2 beta model)
		egon: normal hl1
		hivehand: normal hl1

	grenades:
		frag: normal hl2
		slam: normal hl2:dm
		snark: normal hl1

	tools:
		physcannon: normal hl2
		portalgun: normal portal
		bugbait: normal hl2


to-be-implemneted:
	melee:
		leadpipe: like crowbar but very slow but MASSIVE damage
		knife: like the one from CS:S (pri quick stab, sec heavy slow stab)
		harpoon: pri stab, sec throw
		wrench: pri slow swing, sec charge swing (MASSIVE DAMAGE for long charge)
		suitcase: just be a normal crowbar clone, the noveltiy of whacking people with the suitcase gives it more then enough point to exist
		
	onehanded:
		deagle: faster, more ammo in mag but less damage (compare to 357)
		flaregun: fires flare projectile, sets enemy on fire on contact
		usp: sec attack swaps suppressor on and off (on deals less damage but makes no noise)
		mac10: fast smg, but poor accuracy, pistol rounds
		
	lightarms:
		sawnoff: dual barrel close-range wrecking ball that sucks outside of close quarters
		smg2: 3rnd burst fire
		ak47: slow but very accurate
		m4a1: no sil = fast but inaccurate, sil = slow ,less dmg but more accurate, sec swaps sil
		famas: burst fire rifle
		
	heavyarms:
		sniperrifle: scope, accurate, blows people's faces off bla bla IT'S A FUCKING SNIPER WHAT DO YOU THINK
		m249: fires fast as hell, holds a fuck ton of ammo, but good luck hitting anything with it, pushes the player back slightly while using
		sg550: semi-auto version of the sniper rifle that deals less damage but carries much more ammo in mag
		awp: middle ground of 550 and combine sniper
		
	grenade:
		manhack: like snark but sends a manhack instead
		
	tools:
		physconcussion: from TE120
		physgun: ARGG's, same one GMod's is based off of, good for fucking around with cheats
		healgun: heals friendlies+yourself
		foodpackage: alt heal gun that you can't use on other people but heals you a lot more

ideas (may not be implemented): 

	special:
		shockroach: op4 shockroach
		sporelauncher: op4 sporelauncher
		flamethrower: burn stuff
		gatlinggun: m249 but with it's upsides/downsides multiplied x 5 (including the pushing the player back unless they're crouching and being still)
		
	grenades:
		sporenade: sporelauncher grenade except thrown manually
		flashbang: blinds and deathens enemies (making them unable to shoot), mainly for pvp
		