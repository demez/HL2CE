


Dr Breen's Quest for the Holy Grail

Part II:

The Keeper of the Bridge of Death



HL2 Map by JimmyD
Original scene by Monty Python






From the author of Dr Breen's Funky Breakdancing and the first of the Holy Grail maps (also on filefront) comes the next part. I think this'll be the last Monty Python one.


About - Ok, good reaction to the first Monty Python scene, I thought I'd do a more improved one. This one has more speech, more characters, more expressions, and more unfortunate deaths! Plus a better map than the previous one, after all, we need a gorge under the Bridge of Death, no?

Here we have the pivotal moment as King Arthur and his knights attempt to cross the Bridge of Death, but to do so, they must answer three questions each, from the Bridgekeeper...

Starring:
King Arthur - Dr Breen
Sir Lancelot - Barney Calhoun
Sir Galahad - Dr Eli Vance
Sir Bedevere - Civilian Bloke 7
Sir Robin the Brave - Dr Kleiner
and The Keeper of the Bridge of Death - The G Man


This map is not intended to be in anyway derogatory to Monty Python, their films, or many brilliant sketches. Don't use this map in any compilations without asking me first. This map isn't to be sold, either.

Since this'll be the last Grail one, I'm taking suggestions for other sources to take a scene from. Go on, shout it out loud! Know an amusing scene from a TV program or film? Do tell!


Instructions - To use this in HL2, put the contents of "maps" into the HL2 folder of maps
e.g. ...SteamApps\username\half-life 2\hl2\maps

Put the conent of the "scenes" folder into HL2's scenes
e.g. ...SteamApps\username\half-life 2\hl2\scenes

And, finally, drop the folder inside "sounds" into the sound folder. Don't take the sounds out of the "grail2" folder that they're in, when it's inside the sound folder, otherwise the scene won't be able to find the sound.
e.g. ...SteamApps\username\half-life 2\hl2\sound

To play the level, open the console in HL2, then type "map holygrail2.bsp" (no speech marks). The map should start up, and Dr Breen and co should start 



Comments? Criticisms? Suggestions for my next release? e-mail me! jim_o_dearden@hotmail.com

Have fun...